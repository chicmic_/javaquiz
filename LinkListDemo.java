/**
 * Make a node for single link list.
 **/
class LinkNode {
    public LinkNode(int data, LinkNode next){
        this.data = data;
        this.next = next;
    }

    private int data = 0;
    public void setData(int data) {
        this.data = data;
    }
    public int getData() {
        return data;
    }

    private LinkNode next = null;
    public void setNext(LinkNode node) {
        this.next = node;
    }

    public LinkNode getNext() {
        return next;
    }
}

/**
 * Implementation of link list.
 **/
class LinkList {
    LinkNode root               = null;
    /**
     * Insert element into list node @param data
     **/
    public boolean insertNodeWithData(int data) {
        LinkNode node = new LinkNode(data, null);
        if (root == null) {
            root = node;
        } else {
            LinkNode lastNode = root;
            while (lastNode.getNext() != null && (lastNode = lastNode.getNext()) != null);
            lastNode.setNext(node);
        }
        return true;
    }

    /**
     * Delete last element from list
     **/
    public void removeTail() {
        if (root == null) {
            System.out.println("List is empty");
        } else {
            LinkNode prev = root, tempHead = null;
            while (prev.getNext() != null) {
                    tempHead = prev;
                    prev = prev.getNext();
            }
            if (tempHead == null) {
                    root = null;
            } else{
                tempHead.setNext(null);
            }
        }
    }

    /**
     * Delete largest elements greater than @param target
     * initialMode: Initialize first smallest element into list
     **/
    public boolean removeTarget(int target) {
        if (root == null) {
            System.out.println("List is empty");
            return false;
        } else {
            LinkNode currentNode        = root;
            LinkNode lastCheckedNode    = null;
            LinkNode newRootNode        = null;
            while (currentNode != null) {
                if (currentNode.getData() > target) {
                    if(lastCheckedNode != null){
                        lastCheckedNode.setNext(currentNode.getNext());
                    }
                } else {
                    if (newRootNode == null) {
                        newRootNode = currentNode;
                    }
                    lastCheckedNode = currentNode;
                }
                currentNode = currentNode.getNext();
            }
            root = newRootNode;
        }
        return true;
    }

    /**
     * Display list of link list
     **/
    public void display() {
        if (root == null) {
            System.out.println("List is empty");
        } else {
            LinkNode temNode = root;
            while (temNode.getNext() != null) {
                System.out.print(String.valueOf(temNode.getData()+" "));
                temNode = temNode.getNext();
            }
            System.out.println(String.valueOf(temNode.getData()));
        }
    }
}

class LinkListDemo{
    public static void main(String args[]) {
        System.out.println("LinkListDemo.main()");
        LinkListDemo demoTest = new LinkListDemo();
        demoTest.testOperationsWithEmptyList();
        demoTest.testWithSingleNode();
        demoTest.testWithMultipleNodes();
    }
    
    void testOperationsWithEmptyList(){
        System.out.println("LinkListDemo.testOperationsWithEmptyList()");
        LinkList list = new LinkList();
        list.display();
        list.removeTail();
        list.removeTarget(23);
    }
    void testWithSingleNode(){
        System.out.println("LinkListDemo.testWithSingleNode()");
        LinkList list = new LinkList();
        list.display();
        list.insertNodeWithData(10);
        list.display();
        list.removeTail();
        list.display();
        list.insertNodeWithData(10);
        list.display();
        list.removeTarget(23);
        list.display();
        list.insertNodeWithData(25);
        list.display();
        list.removeTarget(23);
        list.display();
    }
    void testWithMultipleNodes(){
        System.out.println("LinkListDemo.testWithMultipleNodes()");
        LinkList list = new LinkList();
        list.insertNodeWithData(10);
        list.insertNodeWithData(20);
        list.insertNodeWithData(30);
        list.insertNodeWithData(-23);
        list.insertNodeWithData(15);
        list.insertNodeWithData(100);
        list.insertNodeWithData(25);
        list.display();
        list.removeTail();
        list.display();
        list.insertNodeWithData(10);
        list.display();
        list.insertNodeWithData(23);
        list.removeTarget(23);
        list.display();
    }
}
